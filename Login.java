package com.programator.demo.zajecia_06_10_2019.ManagerHasel;

import java.util.Objects;

public class Login implements Comparable{
    private String loginName;
    private String password;

    public Login(String loginName, String password) {
        this.loginName = loginName;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int compareTo(Object o) {
        Login login = (Login) o;
        return this.getLoginName().compareTo(login.getLoginName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Login)) return false;
        Login login = (Login) o;
        return loginName.equals(login.loginName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(loginName);
    }
}
