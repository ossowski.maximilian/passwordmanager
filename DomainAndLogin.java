package com.programator.demo.zajecia_06_10_2019.ManagerHasel;

import java.util.Objects;

public class DomainAndLogin implements Comparable {

    private String domainName;
    private String loginName;

    public DomainAndLogin(String domainName, String loginName) {
        this.domainName = domainName;
        this.loginName = loginName;
    }

    public String getDomainName() {
        return domainName;
    }

    public String getLoginName() {
        return loginName;
    }

    @Override
    public int compareTo(Object o) {
        DomainAndLogin data = (DomainAndLogin) o;
        int domainInt = this.domainName.compareTo(data.domainName);
                if(domainInt == 0){
                    return this.loginName.compareTo(data.loginName);
                } else {
                    return domainInt;
                }
    }

    @Override
    public String toString() {
        return "Domain Name ='" + domainName + '\'' +
                ", Login Name ='" + loginName + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DomainAndLogin)) return false;
        DomainAndLogin that = (DomainAndLogin) o;
        return domainName.equals(that.domainName) &&
                loginName.equals(that.loginName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(domainName, loginName);
    }


}
