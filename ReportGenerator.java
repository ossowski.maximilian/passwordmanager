package com.programator.demo.zajecia_06_10_2019.ManagerHasel;

import java.util.Map;

public class ReportGenerator {
    private PasswordDatabase2 passData;


    public ReportGenerator(PasswordDatabase2 passData) {
        this.passData = passData;
    }

    public void printReport() {
        Map mapToShow = passData.getDataForReport();
        printMap(mapToShow);
    }

    public void printMap(Map<DomainAndLogin, String> map) {
        for (Map.Entry<DomainAndLogin, String> entry : map.entrySet()) {
            System.out.println(entry.getKey()
                    + " Password : " + entry.getValue());
        }
    }

    private String generateStringForOutput(Map<DomainAndLogin, String> map){
        String output = "";
        for (Map.Entry<DomainAndLogin, String> entry : map.entrySet()){
            output += (entry.getKey().getDomainName()) + ";" + (entry.getKey().getLoginName()) + ";" + entry.getValue() + "\n";
        }
        return output;
    }

    public String getStringOutput() {
        Map mapToShow = passData.getDataForReport();
        return generateStringForOutput(mapToShow);
    }

}
