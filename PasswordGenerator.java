package com.programator.demo.zajecia_06_10_2019.ManagerHasel;

import java.util.Random;

public class PasswordGenerator {

    private int passwordCharCount = 0;
    //zbior malych liter // 0 - 24
    private final String[] smallLettersBox = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","y","z"};
    //zbior wielkich liter // 0 - 24
    private final String[] bigLettersBox = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","Y","Z"};
    //zbior znakow specjalnych // 0 - 9
    private final String[] specialCharsBox = {"!",",","@","#","$","%","^","&","*","/"};
    //zbior cyfr // 0 - 9
    private final String[] numbersBox = {"1","2","3","4","5","6","7","8","9","0"};

    public void setPasswordCharCount(Integer passwordCharCount){
        this.passwordCharCount = passwordCharCount;
    }

    public String generatePassword(){
        String[][] allCharBox = {smallLettersBox, bigLettersBox, specialCharsBox, numbersBox};
        String password = "";
        for(int i = 0; i < passwordCharCount; i++){
            Random los = new Random();
            Integer selectedBox = los.nextInt(4);
            if(selectedBox.equals(0) || selectedBox.equals(1)){
                password += allCharBox[selectedBox][los.nextInt(25)];
            }
            if(selectedBox.equals(2) || selectedBox.equals(3)){
                password += allCharBox[selectedBox][los.nextInt(10)];
            }
        }
        return password;
    }
}
