package com.programator.demo.zajecia_06_10_2019.ManagerHasel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {
    private Scanner input;
    private boolean endFlag;
    private boolean flagAnswer;
    private Integer chosenNumber;
    private List<String> lines;
//    private PasswordDatabase passwordDatabase;
    private PasswordDatabase2 passwordDatabase2;
    private PasswordGenerator pass;
    private File file;

    Menu() {
        this.input = new Scanner(System.in);
        this.endFlag = true;
        this.flagAnswer = true;
        this.chosenNumber = null;
        this.lines = new ArrayList<>();
//        this.passwordDatabase = new PasswordDatabase();
        this.passwordDatabase2 = new PasswordDatabase2();
        this.pass = new PasswordGenerator();
        this.file = new File("C:\\Max\\passwordData.txt");
    }

    private void showMenu(){
        System.out.println("");
        System.out.println("------------------");
        System.out.println(" Password Manager ");
        System.out.println("------------------");
        System.out.println("1) Return password");
        System.out.println("2) Generate password");
        System.out.println("3) Add password");
        System.out.println("4) Show report");
        System.out.println("5) End program");
        System.out.println("------------------");
        System.out.println("Please enter number: ");
    }

    void startProgram() {

        // Read data from file
        System.out.println("Reading Input form File ...");
        try {
            lines = Files.readAllLines(file.toPath(), Charset.defaultCharset());
        } catch (IOException e) {
            System.out.println("The input file was not found or wrong file content. Continue without data...");
        }
        for(String line : lines) {
            String[] res = line.split(";");
            if(res.length == 3) {
                addNewPasswordToDatabase(res[0], res[1], res[2]);
            }
        }

        while (endFlag) {
            showMenu();
            String userInput = input.next();
            try {
                chosenNumber = Integer.parseInt(userInput);
            } catch (NumberFormatException e) {
                System.out.println("Please enter numeric value!");
                chosenNumber = null;
            }
            if(chosenNumber != null) {
                String domain;
                String login;
                String password;
                switch (chosenNumber) {
                    case 1:
                        System.out.println("Returning password for given data: ");
                        domain = askUserForDomainName();
                        login = askUserForLogin();
                        if(passwordDatabase2.searchForDomainAndLogin(domain, login)){
                            System.out.println("Password for domain: '" + domain + "' and login: '"
                                    + login + "' is: " + passwordDatabase2.getPasswordFromDatabase(domain,login));
                        } else {
                            System.out.println("There is no password for domain: '" + domain + "' and login: '" + login + "'");
                        }
                        break;
                    case 2:
                        System.out.println("Adding random password for given data: ");
                        domain = askUserForDomainName();
                        login = askUserForLogin();
                        Integer passCount = askForPasswordCount();
                        pass.setPasswordCharCount(passCount);
                        password = pass.generatePassword();
                        addNewPasswordToDatabase(domain, login, password);
                        break;
                    case 3:
                        System.out.println("Adding new password for given data: ");
                        domain = askUserForDomainName();
                        login = askUserForLogin();
                        password = askUserForPassword();
                        addNewPasswordToDatabase(domain, login, password);
                        break;
                    case 4:
                        System.out.println("Report: ");
                        ReportGenerator report = new ReportGenerator(passwordDatabase2);
                        report.printReport();

                        break;
                    case 5:
                        endProgram();
                        break;
                    default:
                        System.out.println("Please enter correct number!");
                }
            }
        }
    }

    private void endProgram(){

        try (PrintWriter out = new PrintWriter(file)) {
            ReportGenerator report = new ReportGenerator(passwordDatabase2);
            String output = report.getStringOutput();
            out.println(output);
            System.out.println("Data was saved to file ...");
        } catch (FileNotFoundException e) {
            System.out.println("Error while trying to write data to File!");
            System.out.println("No data saved!");
        }

        endFlag = false;
    }

    private String askUserForDomainName(){
        System.out.println("Please enter Domain Name: ");
        return input.next();
    }

    private String askUserForLogin(){
        System.out.println("Please enter Login: ");
        return input.next();
    }

    private String askUserForPassword(){
        System.out.println("Please enter Password: ");
        return input.next();
    }

    private Integer askForPasswordCount(){
        System.out.println("Please enter number of characters for your password: ");
        Integer passCount = null;
        while (true) {
            String userInput = input.next();
            try {
                passCount = Integer.parseInt(userInput);
            } catch (NumberFormatException e) {
                System.out.println("Please enter numeric value!");
            }
            if (passCount != null) {
                return passCount;
            }
        }
    }

    private void addNewPasswordToDatabase(String domain, String login, String password){
        if(passwordDatabase2.searchForDomainAndLogin(domain, login)){
            System.out.println("Domain: '" + domain + "' and login: '" + login + "' are existent.");
            System.out.println("Do you want to overwrite the password? ");
            System.out.println(" y - for yes");
            System.out.println(" n - for no");
            while (flagAnswer) {
                String answer = input.next();
                switch (answer) {
                    case "y":
                        passwordDatabase2.addPassword(domain, login, password);
                        System.out.println("New password for domain: '" + domain
                                + "' with login: '" + login + "'");
                        flagAnswer = false;
                        break;
                    case "n":
                        System.out.println("Present password for domain: '" + domain
                                + "' and login: '" + login + "' was not changed!");
                        flagAnswer = false;
                        break;
                    default:
                        System.out.println("Please enter correct answer!");
                }
            }
            flagAnswer = true;
        } else {
            passwordDatabase2.addPassword(domain, login, password);
            System.out.println("New password for domain: '" + domain + "' with login: '" + login + "'");
        }
    }

}
