package com.programator.demo.zajecia_06_10_2019.ManagerHasel;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class Domain implements Comparable {
    private String domainName;
    private Set<Login> loginList;

    public Domain(String domainName) {
        this.domainName = domainName;
        this.loginList = new HashSet<>();
    }

    public String getDomainName() {
        return domainName;
    }

    public Set<Login> getLoginList() {
        return loginList;
    }

    public void addNewLogin(String loginName, String password){
        Login newLogin = new Login(loginName, password);
        loginList.add(newLogin);
    }

    public Optional<Login> searchForLogin(String loginName){
        for(Login login : loginList){
            if(login.getLoginName().equals(loginName)){
                return Optional.of(login);
            }
        }
        return Optional.empty();
    }

    @Override
    public int compareTo(Object o) {
        Domain domain = (Domain) o;
        return this.getDomainName().compareTo(domain.getDomainName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Domain)) return false;
        Domain domain = (Domain) o;
        return domainName.equals(domain.domainName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(domainName);
    }
}
