Password Manager

Specification:

Write a password manager application. The program should have the following functionality:

- After entering the domain address and the user name the program should return the saved password
- If there is not domain/login existing, the user gets an according message
- The program can generate a random password (random alphanumeric password)
- The user can add a password for the given domain/user manually
    - Duplicates should be rejected by the program. If an entry exists the program should ask the user if he wants to overwrite it
- The program can show a full report, sorted by domain address and login. The report shows the domain, user login and the password.
- All the data should be stored in the program memory
    - Additional: data stored in a file, after the start if the file exists, the program loads the saved data from the file

Architecture:
- basic console Application

Technologies/Tools:
- OOP