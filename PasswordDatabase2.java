package com.programator.demo.zajecia_06_10_2019.ManagerHasel;

import java.util.Map;
import java.util.TreeMap;

public class PasswordDatabase2 {

    private Map<DomainAndLogin, String> passwordBase;

    public PasswordDatabase2() {
        this.passwordBase = new TreeMap<>();
    }

    public Map getDataForReport(){
        return passwordBase;
    }

    public void addPassword(String domain, String login, String password){
        DomainAndLogin keyValue = new DomainAndLogin(domain, login);
        passwordBase.put(keyValue, password);
    }

    public String getPasswordFromDatabase(String domain, String login){
        DomainAndLogin searchedData = new DomainAndLogin(domain, login);
        return passwordBase.get(searchedData);
    }

    public boolean searchForDomainAndLogin(String domain, String login){
        DomainAndLogin searchedData = new DomainAndLogin(domain, login);
        return passwordBase.containsKey(searchedData);
    }


}
