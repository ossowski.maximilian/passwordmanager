package com.programator.demo.zajecia_06_10_2019.ManagerHasel;

import java.util.HashSet;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;

public class PasswordDatabase {
    private Set<Domain> domainList;
    private Scanner input;

    public PasswordDatabase() {
        this.domainList = new HashSet<>();
        this.input = new Scanner(System.in);
    }

    public void addPassword(String domainName, String userLogin, String password){
        boolean flagAnswer = true;
        Optional<Domain> domain = searchForDomain(domainName);
        if(domain.isPresent()){
            Optional<Login> login = domain.get().searchForLogin(userLogin);

            if(login.isPresent()){
                System.out.println("Do you want to overwrite the existing password? ");
                System.out.println(" y - for yes");
                System.out.println(" n - for no");
                while (flagAnswer) {
                    String answer = input.next();
                    switch (answer) {
                        case "y":
                            login.get().setPassword(password);
                            System.out.println("New password for domain: " + domainName
                                    + " with login: " + userLogin);
                            flagAnswer = false;
                            break;
                        case "n":
                            System.out.println("Present password for domain: " + domainName
                                    + " and login: " + userLogin + " was not changed!");
                            flagAnswer = false;
                            break;
                        default:
                            System.out.println("Please enter correct answer!");
                    }
                }

            } else {
                domain.get().addNewLogin(userLogin, password);
                System.out.println("New password for domain: " + domainName + " with login: " + userLogin);
            }
        } else {
            Domain newDomain = addNewDomain(domainName);
            newDomain.addNewLogin(userLogin, password);
            System.out.println("New password for domain: " + domainName + " with login: " + userLogin);
        }
    }

    public void findPasswordInDatabase(String domainName, String userLogin){
        Optional<Domain> domain = searchForDomain(domainName);
        if(domain.isPresent()){
            Optional<Login> login = domain.get().searchForLogin(userLogin);
            if(login.isPresent()){
                System.out.println("Password for domain: " + domainName + " and login: "
                + userLogin + " is: " + login.get().getPassword());
            } else {
                System.out.println("There is no login: " + userLogin + " in domain: " + domainName);
            }
        } else {
            System.out.println("There is no domain: " + domainName + " in the database");
        }
    }

//    public void generateReport(){
////        ReportGenerator report = new ReportGenerator(domainList);
////        report.printReport();
////    }

    private Optional<Domain> searchForDomain(String domainName){
        for(Domain domain : domainList){
            if(domain.getDomainName().equals(domainName)){
                return Optional.of(domain);
            }
        }
        return Optional.empty();
    }

    private Domain addNewDomain(String domainName){
        Domain newDomain = new Domain(domainName);
        domainList.add(newDomain);
        return newDomain;
    }
}
